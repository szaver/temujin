// trying to figure out a more elegant way to add colors
macro_rules! h1 {
    ( $( $x:expr ),* ) => {
        println!("{}", format!($($x, )*).red().bold().underline());
    };
}

macro_rules! h2 {
    ( $( $x:expr ),* ) => {
        println!("{}", format!($($x, )*).blue().bold().underline());
    };
}

// macro_rules! h3 {
//     ( $( $x:expr ),* ) => {
//         println!("{}", format!($($x, )*).green().bold().underline());
//     };
// }
