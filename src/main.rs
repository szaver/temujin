extern crate csv;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate clap;
extern crate term_table;
#[macro_use]
extern crate log;
extern crate colored;
extern crate env_logger;
extern crate regex;
#[macro_use]
extern crate if_chain;
extern crate average;

#[macro_use]
mod terminal_formatting;

use average::Mean;
use clap::{App, Arg};
use colored::*;
use regex::RegexBuilder;
use std::error::Error;
use std::fs::File;
use std::io::BufReader;
//use log::Level;

// By default, struct field names are deserialized based on the position of
// a corresponding field in the CSV data's header record.
#[derive(Clone, Debug, Deserialize, PartialEq, Eq)]
struct InputRecord {
    #[serde(rename = "Assignment Name")]
    assignment_name: String,
    #[serde(rename = "Student Name")]
    student_name: String,
    #[serde(rename = "Score At Due Date")]
    score_at_due: Option<u64>,
    #[serde(rename = "Score Best Ever")]
    score_best_ever: Option<u64>,
    #[serde(rename = "Points Possible")]
    points_possible: Option<u64>,
    #[serde(rename = "Number Of Attempts")]
    num_attempts: Option<u64>,
    #[serde(rename = "Most Recent Completion Date")]
    most_recent_completion: Option<String>,
    #[serde(rename = "Due Date")]
    due_date: Option<String>,
    #[serde(rename = "Assignment URL")]
    assignment_url: String,
    #[serde(rename = "Assignment Type")]
    assignment_type: String,
}

impl InputRecord {
    // Entry struch method parsing assignment url and returning unit name
    fn unit_name(&self) -> String {
        /* this seems rather hackish, I'd like to get rid of the magic numbers
        somehow, but I guess if it works than we can run with it. :| */
        let split = self.assignment_url.split(r"/");
        let vec: Vec<&str> = split.collect();
        let unit_string: String = vec[5].to_string();
        // not sure why (i don't understand lifetimes yet), but if I use split
        // again in the line below it complains about temporary values not living
        // long enough
        let split1 = unit_string.split(r"#");
        let unit_name: Vec<&str> = split1.collect();
        debug!("fn unit_name called; {}", unit_name[0].to_string());
        unit_name[0].to_string()

        /* Docs.rs went down in the middle of a coding session, so this is
        My attempt at getting Regex working:

        let mut re = Regex::new(r"(?<=math\/)(.*?)(?=\/)").unwrap();
        if !(&self.assignment_url.is_empty()) {
            let matches: Vec<_> = re.matches(&self.assignment_url).into_iter().collect();
            debug!("fn unit_name called, course_name length is : {}", matches[0])
            //re = Regex::new(r"(?<=" + course_name + r"\/)(.*?)(?=\/)").unwrap();
        } else {
            ""
        }*/
    }
}

fn main() -> Result<(), Box<Error>> {
    let matches = App::new("Temujin")
        .version("1.0")
        // crate_authors macro automatically pulls authors form the Cargo.toml file at compile time
        .author(crate_authors!("\n"))
        .about("Query Khan Academy CSV Files")
        .arg(
            Arg::with_name("input")
                .short("i")
                .long("input")
                .value_name("FILE")
                .help("Input file path")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("student")
                .short("s")
                .long("student")
                .help("Student's Name to sort by")
                .value_name("STUDENT")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("roster")
                .short("r")
                .long("roster")
                .help("List all students in roster"),
        )
        .arg(
            Arg::with_name("list-units")
                .short("l")
                .long("list-units")
                .help("List all units in file"),
        )
        .arg(Arg::with_name("assignments").short("a").long("assignments"))
        .arg(
            Arg::with_name("grade")
                .short("g")
                .long("grade")
                .help("parses and returns grades"),
        )
        .get_matches();

    // Basic logging functionality: read up on env_logger for use
    env_logger::init();

    /* Example logging code
    debug!("this is a debug {}", "message");
    error!("this is printed by default");

    if log_enabled!(Level::Info) {
        let x = 3 * 4; // expensive computation
        info!("the answer was: {}", x);
    }*/

    // read and parse entries
    // collect cli flags
    let input_file_name = matches.value_of("input").unwrap();
    // "." Will match anything but a newline
    let student_filter_name = matches.value_of("student").unwrap_or(".");
    debug!("Filtering by Student: {}", student_filter_name);

    // init regex (better to do it here than on every loop iteration)
    let student_name_regex = RegexBuilder::new(student_filter_name)
        .case_insensitive(true)
        .build()
        .expect("Invalid student regular expression!");

    // CSV reader
    let input_file = File::open(input_file_name)?;
    let mut reader = csv::Reader::from_reader(BufReader::new(input_file));

    // variable to hold all entries
    let mut entries: Vec<InputRecord> = Vec::new();

    for result in reader.deserialize() {
        let record: InputRecord = result?;

        // TODO: Create method returning grade. This will be assignment grade,
        // not unit grade, as each entry represents an assignment, not a unit.

        if student_name_regex.is_match(&record.student_name) {
            entries.push(record)
        }
    }
    // filter entries according to cli flags
    // get list of units
    debug!("Listing units: {}", matches.is_present("list-units"));
    let mut unit_names: Vec<String> = Vec::new();
    for entry in &entries {
        unit_names.push(entry.unit_name());
    }
    unit_names.sort();
    unit_names.dedup();

    // get list of students
    debug!("Listing Students: {}", matches.is_present("roster"));
    let mut student_names: Vec<String> = Vec::new();
    for entry in &entries {
        let name = &entry.student_name;
        student_names.push(name.to_string());
    }
    // case-insensitive sort
    student_names.sort_by(|a, b| a.to_lowercase().cmp(&b.to_lowercase()));
    student_names.dedup();

    // note, students are filtered during deserialization, see above
    if matches.is_present("list-units") {
        println!("List of Units:");
        for unit in &unit_names {
            println!("Unit: {}", unit);
        }
    }

    if matches.is_present("roster") {
        println!("List of Students:");
        for student in &student_names {
            println!("Student: {}", student);
        }
    }

    if matches.is_present("grade") {
        for unit in &unit_names {
            h1!("\n{}", unit);

            for student_name in &student_names {
                let mut exercises: Vec<&InputRecord> = Vec::new();
                let mut unit_tests: Vec<&InputRecord> = Vec::new();
                for entry in &entries {
                    // filter by student and unit
                    if (&entry.student_name == student_name) && (&entry.unit_name() == unit) {
                        // separate based on type
                        if entry.assignment_type == "Exercise" {
                            exercises.push(&entry);
                        } else if entry.assignment_type == "Unit Test" {
                            unit_tests.push(&entry);
                        }
                    }
                }

                // first try getting score from exersizes
                let mut method = "Ex";
                let mut score = mean_score(&exercises);
                if score.is_none() {
                    method = "Te";
                    score = mean_score(&unit_tests);
                }

                // print the score
                match score {
                    None => println!("        {}", student_name.dimmed()),
                    Some(grade) => {
                        let mut score_str = format!("{: >3.0}%", grade * 100.0).yellow();
                        if grade > 0.8 {
                            score_str = score_str.green();
                        }
                        if grade < 0.5 {
                            score_str = score_str.red();
                        }

                        println!("{} {} {}", method, score_str, student_name)
                    }
                }
            }
        }
    }

    if matches.is_present("assignments") {
        for unit in &unit_names {
            h1!("\n\nUnit: {}", unit);

            let mut none_found_for: Vec<&str> = Vec::new();

            for student in &student_names {
                // create a new vector only containing matching assignments for this student
                let matching_assignments: Vec<&InputRecord> = entries
                    .iter()
                    .filter(|entry| {
                        (&entry.student_name == student) && (&entry.unit_name() == unit)
                    })
                    .collect();

                if matching_assignments.is_empty() {
                    none_found_for.push(&student)
                } else {
                    h2!("\n\nStudent: {}", student);

                    for entry in &matching_assignments {
                        // format the score separately so we can style it as a block
                        // TODO: maybe color code this?
                        let score = format!(
                            "{:0>2}/{:0>2}",
                            entry.score_best_ever.unwrap_or(0),
                            entry.points_possible.unwrap_or(1),
                        );

                        println!(
                                "{score} {name} {type}",
                                score = score.dimmed().bold(),
                                name = entry.assignment_name,
                                type = entry.assignment_type.dimmed(),
                            )
                    }
                }
            }

            h2!("\n\nNo assignments found for:");
            for student in none_found_for {
                println!("- {}", student);
            }
        }
    }

    Ok(())
}

fn mean_score(assignments: &Vec<&InputRecord>) -> Option<f64> {
    let valid_assignments: Vec<f64> = assignments
        .iter()
        .filter_map(|ex| {
            if_chain! {
                if let Some(best_ever) = ex.score_best_ever;
                if let Some(possible) = ex.points_possible;
                if possible != 0;
                then {
                    return Some(best_ever as f64 / possible as f64);
                }
            }
            None
        })
        .collect();

    if valid_assignments.is_empty() {
        return None;
    }

    let estimator: Mean = valid_assignments.iter().collect();
    Some(estimator.mean())
}
